FROM alpine:latest
RUN apk add --no-cache git curl tar

ADD ./scripts/* /usr/sbin
